FROM composer:1.8.3 as composer
FROM php:7.3-fpm-alpine

RUN set -ex \
\
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && apk add --no-cache \
        git  \
        openssh-client \
        ca-certificates \
        icu \
        zlib \
        shadow \
        libzip \
        postgresql-libs \
		fcgi \
        libressl \
    && apk add --no-cache --virtual .build-deps \
        tar \
        xz \
        curl \
        ${PHPIZE_DEPS} \
        python \
        bash \
        git \
        icu-dev \
		zlib-dev \
		libzip-dev \
		postgresql-dev \
		icu-dev \
		zlib-dev \
		libzip-dev \
		postgresql-dev \
\
    && pecl install apcu xdebug \
    && docker-php-ext-enable apcu \
    && docker-php-ext-install intl \
    && docker-php-ext-install opcache \
    && docker-php-ext-enable intl \
    && docker-php-ext-install zip \
    && docker-php-ext-enable zip \
    && docker-php-ext-install pgsql \
    && docker-php-ext-install pdo_pgsql \
\
    && curl --location --output /usr/local/bin/php-cs-fixer "http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar" \
    && chmod +x /usr/local/bin/php-cs-fixer \
\
    && apk del .build-deps \
    && rm -rf /tmp/*


RUN version=$(php -r "echo PHP_MAJOR_VERSION.PHP_MINOR_VERSION;") \
    && curl -A "Docker" -o /tmp/blackfire-probe.tar.gz -D - -L -s https://blackfire.io/api/v1/releases/probe/php/alpine/amd64/$version \
    && mkdir -p /tmp/blackfire \
    && tar zxpf /tmp/blackfire-probe.tar.gz -C /tmp/blackfire \
    && mv /tmp/blackfire/blackfire-*.so $(php -r "echo ini_get('extension_dir');")/blackfire.so \
    && printf "extension=blackfire.so\nblackfire.agent_socket=tcp://blackfire:8707\n" > $PHP_INI_DIR/conf.d/blackfire.ini \
    && rm -rf /tmp/blackfire /tmp/blackfire-probe.tar.gz

COPY --from=composer /usr/bin/composer /usr/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN set -eux; \
    composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --classmap-authoritative; \
    composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"

COPY ./php.ini /usr/local/etc/php/php.ini
COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf

COPY ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint
ENTRYPOINT ["docker-entrypoint"]

CMD ["php-fpm"]

WORKDIR /srv/app
