#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
    set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'bin/console' ]; then
    # Detect the host IP
    export DOCKER_BRIDGE_IP
    DOCKER_BRIDGE_IP=$(ip ro | grep default | cut -d' ' -f 3)

    # Prepare www-data
    if [ -n "${GROUP}" ] ; then
        groupmod --gid "${GROUP}" --non-unique www-data
    fi

    if [ -n "${USER}" ] ; then
        usermod --non-unique --uid "${USER}" www-data
    fi

    if [ -n "${USER}" ] || [ -n "${GROUP}" ] ; then
        chown -R www-data:www-data ./
    fi
fi

exec docker-php-entrypoint "$@"
