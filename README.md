# PHP-FPM

Image PHP-FPM utilisée par les applications Symfony de la démo Kubernetes. 

## Principe

Les socles Docker ont pour but de faciliter le développement des applications sous docker et leur déploiement sous Kubernetes.
Nous utilisons cette image aussi bien pour du *php-fpm* via *nginx* que pour lancer de traitements.

Cette image n'a  pas pour but d'être partagée ou utilisée par d'autres, mais peut servir d'exemple pour créer votre propre socle. 
Elle intègre : 

* **PHP 7.3** : En raison de la possibilité de gérer des logs JSON sur *stdout* sans limite de taille et sans decorator. (`decorate_workers_output` et `log_limit`) 
* **PostgreSQL**
* **Blackfire**


## CI

L'image est buildée par Gitlab-CI : 
* `latest` est buildée lors d'un push sur *master*
* les `tags` sont buildés lorsqu'un tag est poussé sur git.

A terme cette image pourrait être buildée et testée sur des patterns de branches avant d'être mergée dans master.


